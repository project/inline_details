<?php

namespace Drupal\inline_details\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provide a (( )) text filter for inline details.
 *
 * @Filter(
 *   id = "inline_details",
 *   title = @Translation("Inline Details"),
 *   description = @Translation("Provide a (( )) text filter for inline details."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class InlineDetailsFilter extends FilterBase {

  /**
   * @inheritDoc
   */
  public function process($text, $langcode) {
    // unicode, multiline, dotall.
    $pattern = '#\\(\\((.*?)\\)\\)#usm';
    $result = new FilterProcessResult('');
    $callback = function ($matches) use ($result) {
      $return = sprintf('<label class="inline-details">(<input type="checkbox"><span class="inline-details-placeholder">…</span><span class="inline-details-content">%s</span>)</label>', $matches[1]);
      $result->addAttachments(['library' => ['inline_details/library']]);
      return $return;
    };
    $new_text = preg_replace_callback($pattern, $callback, $text, -1, $count);
    $result->setProcessedText($new_text);
    return $result;
  }

}
